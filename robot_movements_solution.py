grid = [[0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0]]

def walk(grid, currX, currY, endX, endY):
    if currX < 0 or currX > 3 or currY < 0 or currY > 3:
        return 0

    if grid[currX][currY]:
        return 0

    if currX == endX and currY == endY:
        return 1

    grid[currX][currY] = 1

    res = 0

    res += walk(grid, currX+1, currY, endX, endY)
    res += walk(grid, currX, currY+1, endX, endY)
    res += walk(grid, currX-1, currY, endX, endY)
    res += walk(grid, currX, currY-1, endX, endY)

    grid[currX][currY] = 0

    return res

def evaluate(beginX, beginY, endX, endY):
    return walk(grid, beginX, beginY, endX, endY)


print evaluate(0, 0, 3, 3)