import unittest
import robot_movements_solution


class TestRobotMovementChallenge(unittest.TestCase):

    def test_evaluate_case1(self):
        result = robot_movements_solution.evaluate(beginX=0, beginY=0,
                                                   endX=0, endY=0)
        assert result == 1


    def test_evaluate_case2(self):
        result = robot_movements_solution.evaluate(beginX=0, beginY=0,
                                                   endX=3, endY=3)
        assert result == 184

    def test_evaluate_case3(self):
        result = robot_movements_solution.evaluate(beginX=0, beginY=0,
                                                   endX=2, endY=2)
        assert result == 132

    def test_evaluate_case4(self):
        result = robot_movements_solution.evaluate(beginX=0, beginY=0,
                                                   endX=0, endY=1)
        assert result == 98

