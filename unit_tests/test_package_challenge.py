import unittest
import package_challenge_sollution


class TestPackageChallenge(unittest.TestCase):

    def test_evaluate_case1(self):
        result = package_challenge_sollution.evaluate(weight_limit=81,
                                   items=[(1,53.38, 45), (2,88.62, 98),
                                          (3,78.48,3), (4,72.30,76),
                                          (5,30.18,9), (6,46.34,48)])
        assert result == [4]

    def test_evaluate_case2(self):
        result = package_challenge_sollution.evaluate(weight_limit=8,
                                   items=[(1,15.3, 34)])
        assert result == '-'


    def test_evaluate_case3(self):
        result = package_challenge_sollution.evaluate(weight_limit=75,
                                   items=[(1, 85.31, 29), (2, 14.55, 74),
                                          (3, 3.98, 16), (4, 26.24, 55),
                                          (5, 63.69, 52), (6, 76.25, 75),
                                          (7, 60.02, 74), (8, 93.18, 35),
                                          (9, 89.95, 78)])
        assert result == [2, 7]

    def test_evaluate_case4(self):
        result = package_challenge_sollution.evaluate(weight_limit=56,
                                   items=[(1,90.72, 13), (2,33.80, 40),
                                          (3,43.15, 10), (4,37.97, 16),
                                          (5,46.81, 36), (6,48.77, 79),
                                          (7,81.80, 45), (8,19.36, 79),
                                          (9,6.76, 64)])
        assert result == [8, 9]

    def test_evaluate_case5(self):
        result = package_challenge_sollution.evaluate(weight_limit=58,
                                                      items=[(1,13.73,70),
                                                             (2,46.54,92),
                                                             (3,15.11,31),
                                                             (4,14.73,77),
                                                             (5,73.69,2),
                                                             (6,14.77,55),
                                                             (7,11.20,85),
                                                             (8,60.25,80)])
        assert result == [1, 4, 6, 7]
