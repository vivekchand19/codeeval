**Description**

Solution to https://www.codeeval.com/public_sc/114/ (Package Problem)

```
#!bash
vivek@vivek-desktop:~/projects/dailyrounds$ python package_challenge_sollution.py data.txt 
4
-
2,7
8,9
```

Solution to https://www.codeeval.com/public_sc/56/ (Robot Movements)

```
#!bash
vivek@vivek-desktop:~/projects/dailyrounds$ python robot_movements_solution.py
184
```

Have ran these on https://www.codeeval.com