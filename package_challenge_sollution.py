import sys

def evaluate(weight_limit, items):
    # consider those items whose weight is less than weight limit
    items_to_consider =  [Item(index=item[0],
                               weight=item[1],
                               price=item[2])
                          for item in items if
                          item[1] < weight_limit
                          and item[2] <= 100 and item[1] <= 100]
    if not items_to_consider:
        return '-'

    items_to_consider.sort(cmp=compare_items)
    sorted_by_relevance = items_to_consider

    possible_packages = []
    for item in sorted_by_relevance:
        package = Package(weight_limit=weight_limit)
        package.add_item(item)

        for other_item in sorted_by_relevance:
            if item != other_item and package.can_add_item(other_item):
                package.add_item(other_item)
        possible_packages.append(package)

    possible_packages.sort(cmp=compare_package)
    best_package = possible_packages[0]
    return sorted([item.index for item in best_package.items])

# the below objects are Item & Package entities, helps in better abstraction
class Item(object):
    def __init__(self, index, weight, price):
        self.index = index
        self.weight = weight
        self.price = price

class Package(object):
    def __init__(self, weight_limit):
        self.weight_limit = weight_limit

        self.items = []
        self.total_price = 0.0 # in $
        self.total_weight = 0.0

    def can_add_item(self, item):
        return True if \
            self.total_weight + item.weight < self.weight_limit \
            else False

    def add_item(self, item):
        self.items.append(item)
        self.total_weight += item.weight
        self.total_price += item.price

def compare_package(package1, package2):
    # sort packages by relevance, will have the highest in price first
    # if 2 packages are of same price, the one with lesser weight will be first
    if package1.total_price == package2.total_price:
        if package1.total_weight == package2.total_weight:
            return 0
        if package1.total_weight < package2.total_weight:
            return -1
        else:
            return 1
    return -1 if package1.total_price > package2.total_price else 1

def compare_items(item1, item2):
    # sort items by relevance, will have the highest in price first
    # if 2 items are of same price, the one with lesser weight will be first
    if item1.price == item2.price:
        if item1.weight == item2.weight:
            return 0
        if item1.weight < item2.weight:
            return -1
        else:
            return 1
    return -1 if item1.price > item2.price else 1

if __name__ == '__main__':
    # File parsing logic
    file_path = sys.argv[1]
    file = open(file_path, 'r')

    def _parse_item(item_data):
        index, weight, price = item_data.split(',')
        return int(index), float(weight), int(price.split('$')[1])

    for line in file.readlines():
        if not line:
            continue
        weight_limit = float(line.split(':')[0])
        items = []
        item_begin = False
        for char in line.split(':')[1]:
            if char == '(':
                item_data = u''
                item_begin = True
                continue
            if char == ')':
                items.append(_parse_item(item_data))
                item_begin = False
            if item_begin:
                item_data += char
        items = evaluate(weight_limit=weight_limit, items=items)
        print u','.join([str(item) for item in items])
